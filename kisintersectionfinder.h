#ifndef KISINTERSECTIONFINDER_H
#define KISINTERSECTIONFINDER_H

#include <QPointF>
#include "bezier.h"
#include "databuffer.h"
#include "painterpath.h"
#include "painterpath_p.h"
#include "numericalengine.h"
#include "pathclipper.h"
#include "vectorpath.h"

enum buildingBlockElementType {
    MoveToElement,
    LineToElement,
    CurveToElement,
//        CurveToDataElement
};

class buildingBlock {
public:

    friend class KisIntersectionFinder;

    buildingBlock(int id, buildingBlockElementType buildingblocktype, Line l1) : type(buildingblocktype), l(l1) {


    }

    buildingBlock(int id, buildingBlockElementType buildingblocktype, cubicBezier c1) : type(buildingblocktype) {

        cb = c1;
    }

    bool isMoveTo() const { return type == MoveToElement; }
    bool isLineTo() const { return type == LineToElement; }
    bool isCurveTo() const { return type == CurveToElement; }

    QPointF getMoveTo() {
        if (type == MoveToElement) {
            return moveToPoint;
        }
    }

    Line getLineTo() {
        if (type == LineToElement) {
            return l;
        }
    }

    cubicBezier getCurveTo() {
        if (type == CurveToElement) {
            return cb;
        }
    }



private:

    int ID;
    buildingBlockElementType type;
    Line l;
    cubicBezier cb;
    QPointF moveToPoint;

};

class KisIntersectionFinder {
public:
    KisIntersectionFinder(){};
    KisIntersectionFinder(MyPainterPath subject, MyPainterPath clip);
    ~KisIntersectionFinder(){};
    friend class cubicBezier;
    friend class buildingBlock;
    QVector<QPointF> intersectionPoint(cubicBezier c1, cubicBezier c2);
    QVector<QPointF> intersectionPoint(Line l1, cubicBezier c2);
    QVector<QPointF> intersectionPoint(cubicBezier c1, Line l2);
    QVector<QPointF> intersectionPoint(Line l1,Line l2);
    bool linesIntersect(const QLineF &a, const QLineF &b) const;
    QVector<QPointF> findAllIntersections();




    QVector<QPointF> findIntersectionPoints(buildingBlock e1, buildingBlock e2);

//private:

    QVector<buildingBlock> subjectShape;
    QVector<buildingBlock> clipShape;
    QVector<buildingBlock> netShape;

//    KoRTree<cubicBezier> tree;

};

#endif // KISINTERSECTIONFINDER_H
