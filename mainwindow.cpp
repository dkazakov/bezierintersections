#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QGraphicsScene>
#include <QPainterPath>
#include <QPen>
#include <QBrush>
#include "pathclipper.h"
#include "painterpath.h"
#include "painterpath_p.h"
#include <QWheelEvent>
#include <QGraphicsView>

#include "numericalengine.h"
#include "kisintersectionfinder.h"
#include <QGraphicsSceneMouseEvent>





MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setMouseTracking(true);
    std::cout.precision(10);
}

MainWindow::~MainWindow()
{
    delete ui;
}


MyPainterPath kisUnite(MyPainterPath p1, MyPainterPath p2){
    KisPathClipper clipper(p1, p2);
    return clipper.clip(KisPathClipper::BoolOr);
}

MyPainterPath kisSubtract(MyPainterPath p1, MyPainterPath p2){
    KisPathClipper clipper(p1, p2);
    return clipper.clip(KisPathClipper::BoolSub);
}

MyPainterPath kisIntersect(MyPainterPath p1, MyPainterPath p2){
    KisPathClipper clipper(p1, p2);
    return clipper.clip(KisPathClipper::BoolAnd);
}

void MainWindow::on_graphicsView_rubberBandChanged(const QRect &viewportRect, const QPointF &fromScenePoint, const QPointF &toScenePoint){

}

void MainWindow::wheelEvent(QWheelEvent *event)
    {
        if (event->modifiers() & Qt::ControlModifier) {
            // zoom
            const QGraphicsView::ViewportAnchor anchor = ui->graphicsView->transformationAnchor();
            ui->graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
            int angle = event->angleDelta().y();
            qreal factor;
            if (angle > 0) {
                factor = 1.1;
            } else {
                factor = 0.9;
            }
            ui->graphicsView->scale(factor, factor);
            ui->graphicsView->setTransformationAnchor(anchor);
        }
//        else {
//            ui->graphicsView->wheelEvent(event);
//        }
    }

// for curve-curve intersection
void MainWindow::on_pushButton_clicked()
{
    scene.clear();
    ui->graphicsView->setScene(&scene);

    MyPainterPath qpp;

    QPointF cp0(20, 20); //(4, 1);
    QPointF cp1(40, 260); //(8, 4);
    QPointF cp2(60, -160); //(6, 7);
    QPointF cp3(80,80); //(2, 6);



//    QPointF cba(5, 25); //{ 3,3 };
//    QPointF cbb(300, 35); //{ 8,1 };
//    QPointF cbc(-180, 60); //{ 12,2 };
//    QPointF cbd(100, 70); //{ 11,5 };

//    QPointF cp0(-40, 20); //(4, 1);
//    QPointF cp1(600, 200); //(8, 4);
//    QPointF cp2(-200,200); //(6, 7);
//    QPointF cp3(120, 20); //(2, 6);



    QPointF cba(90, 25); //{ 3,3 };
    QPointF cbb(300, 35); //{ 8,1 };
    QPointF cbc(-180, 60); //{ 12,2 };
    QPointF cbd(100, 70); //{ 11,5 };


    cubicBezier cb1 {cp0,cp1,cp2,cp3};
    cubicBezier cb2 {cba, cbb, cbc, cbd};

    MyPainterPath cp;
    MyPainterPath cpp;
    MyPainterPath overLimits;

    cp.addEllipse(cp0,1,1);
    cp.addEllipse(cp1,1,1);
    cp.addEllipse(cp2,1,1);
    cp.addEllipse(cp3,1,1);


    cpp.addEllipse(cba,1,1);
    cpp.addEllipse(cbb,1,1);
    cpp.addEllipse(cbc,1,1);
    cpp.addEllipse(cbd,1,1);

//    for (int i = 0; i < 400; i++) {
//        overLimits.addEllipse(QPointF(cb1.getParametricX().evaluate( 0 - (i * 0.01)), cb1.getParametricY().evaluate(0- (i * 0.01))),1,1);
//    }

    KisIntersectionFinder KIF;
    QVector<QPointF> intersectionPoints =  KIF.intersectionPoint(cb1, cb2);

    MyPainterPath intersectionPointsPaths;

    Q_FOREACH(QPointF p, intersectionPoints) {

        intersectionPointsPaths.addEllipse(p,1,1);
//        std::cout << "QPointF( " << p.x() << ", " << p.y() << " ), ";

    }


    MyPainterPath c1;
    c1.moveTo(cp0);
    c1.cubicTo(cp1, cp2, cp3);

    MyPainterPath c2;
    c2.moveTo(cba);
    c2.cubicTo(cbb, cbc, cbd);

    QPen pen;
    pen.setWidthF(1);
    pen.setColor(QColor("teal"));

    QPen pen2;
    pen2.setWidthF(1);
    pen2.setColor(QColor("pink"));

    QPen pen3;
    pen3.setWidthF(1);
    pen3.setColor(QColor("yellow"));

    QPen pen4;
    pen4.setWidthF(2);
    pen4.setColor(QColor("red"));

    QPen pen5;
    pen5.setWidthF(2);
    pen5.setColor(QColor("green"));

    QPen pen6;
    pen6.setWidthF(50);
    pen6.setColor(QColor("black"));

    scene.addPath(c1.toQPainterPath(),pen);
    scene.addPath(c2.toQPainterPath(),pen2);
    scene.addPath(intersectionPointsPaths.toQPainterPath(), pen3);
    scene.addPath(cp.toQPainterPath(), pen4);
    scene.addPath(cpp.toQPainterPath(), pen5);


//    scene.addPath(overLimits.toQPainterPath(), pen6);
    ui->graphicsView->repaint();



}





void MainWindow::on_label_linkActivated(const QString &link)
{

}

// for line-curve intersection
void MainWindow::on_pushButton_2_clicked()
{
    scene.clear();
    ui->graphicsView->setScene(&scene);

    MyPainterPath qpp;

    // 45 100   45 69.62433876100000418318814   69.62433876100000418318814 45   100 45

    QPointF cp0(45,100); //(4, 1);
    QPointF cp1(45, 69.62); //(8, 4);
    QPointF cp2(69.62,45); //(6, 7);
    QPointF cp3(100, 45); //(2, 6);

    cubicBezier cb1 {cp3, cp2, cp1, cp0};

    // curveTo 100 45   130.3756612390000100276666 45   155 69.62433876100000418318814   155 100

    QPointF cpa(100, 45); //{ 3,3 };
    QPointF cpb(130.3756, 45); //{ 8,1 };
    QPointF cpc(155, 69.6243); //{ 12,2 };
    QPointF cpd(155, 100); //{ 11,5 };

    cubicBezier cb2 {cpd, cpc, cpb, cpa};

    MyPainterPath line;

    double yC = 45.5;

    QLineF qL(QPointF(50,yC), QPointF(150, yC));
    Line l1(qL);

    QLineF qL2(QPointF(50,50), QPointF(50, 100));
    Line l2(qL2);

//    QVector<double> gg = l1.findRoots(cb1);
//    QVector<double> gg2 = l2.findRoots(cb1);

    KisIntersectionFinder KIF;

    QVector<QPointF> intPoints = KIF.intersectionPoint(l1, cb1);
    QVector<QPointF> intPoints2 = KIF.intersectionPoint(l2, cb1);

    QVector<QPointF> intPoints3 = KIF.intersectionPoint(l1, cb2);

    MyPainterPath intersectionPointsPaths;

    Q_FOREACH(QPointF p, intPoints) {
        intersectionPointsPaths.addEllipse(p,1,1);
        std::cout.precision(10);
        std::cout << "QPointF( " << p.x() << ", " << p.y() << " ), ";

    }

    MyPainterPath intersectionPointsPaths2;

    Q_FOREACH(QPointF p, intPoints2) {
        intersectionPointsPaths2.addEllipse(p,1,1);
    }

    MyPainterPath intersectionPointsPaths3;

    Q_FOREACH(QPointF p, intPoints3) {
        intersectionPointsPaths3.addEllipse(p,0.5,0.5);
    }

    MyPainterPath boundingbox;

    QRectF bb = cb1.boundingBox();
    boundingbox.addRect(bb);

    MyPainterPath c1;
    c1.moveTo(cp0);
    c1.cubicTo(cp1, cp2, cp3);

    MyPainterPath cbbb2;
    c1.moveTo(cpa);
    c1.cubicTo(cpb, cpc, cpd);

    MyPainterPath c2;
    c2.moveTo(QPointF(qL.p1()));
    c2.lineTo(qL.p2());

    MyPainterPath c3;
    c3.moveTo(qL2.p1()); //QPointF(40,20), QPointF(40, 180)
    c3.lineTo(qL2.p2());

    QPen pen;
    pen.setWidthF(0.3);
    pen.setColor(QColor("teal"));

    QPen pen2;
    pen2.setWidthF(0.3);
    pen2.setColor(QColor("pink"));

    QPen pen3;
    pen3.setWidthF(1);
    pen3.setColor(QColor("black"));

    QPen pen4;
    pen4.setWidthF(1);
    pen4.setColor(QColor("red"));

    QPen pen5;
    pen5.setWidthF(1);
    pen5.setColor(QColor("cyan"));

    scene.addPath(c1.toQPainterPath(),pen);
    scene.addPath(cbbb2.toQPainterPath() , pen);
    scene.addPath(c2.toQPainterPath(),pen2);
    scene.addPath(c3.toQPainterPath(),pen2);
    scene.addPath(intersectionPointsPaths.toQPainterPath(), pen3);
    scene.addPath(intersectionPointsPaths2.toQPainterPath(), pen4);
    scene.addPath(intersectionPointsPaths3.toQPainterPath(), pen4);
//    scene.addPath(boundingbox.toQPainterPath(),pen5);

    ui->graphicsView->repaint();

}

void printType(MyPainterPath mp) {
    for(int i = 0; i < mp.elementCount(); i++) {
    MyPainterPath::Element e = mp.elementAt(i);
    int type = e.type;

    std::cout << type << ", ";
//    if(type == 0) {
//        std::cout << "moveTo" << std::endl;
//    }

//    else if(type == 1) {
//        std::cout << "lineTo" << std::endl;
//    }

//    else if(type == 2) {
//        std::cout << "curveTo" << std::endl;
//    }

//    else if(type == 3) {
//        std::cout << "curveToData" << std::endl;
//    }


    }
               }


// for QPainterPath intersections
void MainWindow::on_pushButton_3_clicked()
{
    scene.clear();
    ui->graphicsView->setScene(&scene);

    MyPainterPath qpp;

    MyPainterPath horiRect;
    horiRect.addRoundedRect(50,50,100,100, 20, 20);
    //horiRect.addEllipse(QPointF(70,70),120,50);

    MyPainterPath ellipse;
    ellipse.addEllipse(QPointF(110, 100),79.79, 55);
    //ellipse.addRect(50,18,100,200);

    KisIntersectionFinder KIF(horiRect, ellipse);

    QVector<QPointF> result = KIF.findAllIntersections();

    MyPainterPath markers2;

//    std::cout << "path int points:\n";
    Q_FOREACH(QPointF pt, result) {
//        std::cout.precision(10);
//        std::cout << "QPointF( " << pt.x() << ", " << pt.y() << " ), ";
        markers2.addEllipse(pt,1, 1);
    }

//    MyPainterPath markers;

//    for (int i = 0; i < horiRect.elementCount(); i++) {
//        markers.addEllipse(horiRect.elementAt(i),1,1);
//    }

    MyPainterPath bruh1;
    MyPainterPath bruh2;

    QPointF cp1(30,60);
    QPointF cp2(60,40);
    QPointF cp3(80,40);
    QPointF cp4(120,10);


    Line l1(QPointF(20,50), QPointF(120, 50));
    cubicBezier cb(cp1, cp2, cp3, cp4);



    bruh1.moveTo(20,50);
    bruh1.lineTo(120, 50);

    bruh2.moveTo(cp1);
    bruh2.cubicTo( cp2, cp3, cp4);

    std::cout << "horiRect" << std::endl;
    printType(horiRect);

    for(int i = 0; i < KIF.subjectShape.size(); i++) {
    buildingBlock e = KIF.subjectShape.at(i);

//    if(e.isMoveTo()) {
//        std::cout << "!! moveTo" << std::endl;
//    }

//    else if(e.isLineTo()) {
//        std::cout << "!! lineTo" << std::endl;
//    }

//    else if(e.isCurveTo()) {
//        std::cout << "!! curveTo" << e.getCurveTo().getControlPoints().at(0).x() << " " << e.getCurveTo().getControlPoints().at(0).y()
//                  <<"   " <<  e.getCurveTo().getControlPoints().at(1).x() << " " << e.getCurveTo().getControlPoints().at(1).y()
//                  <<"   " << e.getCurveTo().getControlPoints().at(2).x() << " " << e.getCurveTo().getControlPoints().at(2).y()
//                  <<"   " << e.getCurveTo().getControlPoints().at(3).x() << " " << e.getCurveTo().getControlPoints().at(3).y() << std::endl;
//    }

    }



    std::cout << "ellipse" << std::endl;
    printType(ellipse);

//    for(int i = 0; i < KIF.clipShape.size(); i++) {
//    buildingBlock e = KIF.clipShape.at(i);

//    if(e.isMoveTo()) {
//        std::cout << "!! moveTo" << std::endl;
//    }

//    else if(e.isLineTo()) {
//        std::cout << "!! lineTo" << std::endl;
//    }

//    else if(e.isCurveTo()) {
//        std::cout << "!! curveTo" << e.getCurveTo().getControlPoints().at(0).x() << " " << e.getCurveTo().getControlPoints().at(0).y()
//                  <<"   " <<  e.getCurveTo().getControlPoints().at(1).x() << " " << e.getCurveTo().getControlPoints().at(1).y()
//                  <<"   " << e.getCurveTo().getControlPoints().at(2).x() << " " << e.getCurveTo().getControlPoints().at(2).y()
//                  <<"   " << e.getCurveTo().getControlPoints().at(3).x() << " " << e.getCurveTo().getControlPoints().at(3).y() << std::endl;
//    }

//    else {
//        std::cout << "!! bruhhhhhhhhhhhhhhhhhhhhhhhhhhhh" << std::endl;
//    }

//    }

    KisIntersectionFinder kif2(bruh1, bruh2);
    QVector<QPointF> res2 = kif2.findAllIntersections();

//    Q_FOREACH(QPointF pt, res2) {
//        markers.addEllipse(pt,1, 1);
//    }

    QPen pen;
    pen.setWidthF(0.3);
    pen.setColor(QColor("teal"));

    QPen pen2;
    pen2.setWidthF(0.3);
    pen2.setColor(QColor("pink"));

    QPen pen3;
    pen3.setWidthF(1);
    pen3.setColor(QColor("brown"));

    QPen pen4;
    pen4.setWidthF(1);
    pen4.setColor(QColor("red"));

    QPen pen5;
    pen5.setWidthF(0.5);
    pen5.setColor(QColor("cyan"));

    /*enum ElementType {
        MoveToElement,
        LineToElement,
        CurveToElement,
        CurveToDataElement
    };                      */

 //   scene.addPath(qpp.toQPainterPath());

//    scene.addPath(bruh1.toQPainterPath(),pen);
//    scene.addPath(bruh2.toQPainterPath(),pen2);
//    scene.addPath(markers.toQPainterPath(), pen3);

    scene.addPath(horiRect.toQPainterPath(),pen);
    scene.addPath(ellipse.toQPainterPath(),pen2);
    scene.addPath(markers2.toQPainterPath(), pen3);
//    scene.addPath(markers.toQPainterPath(), pen5);

    ui->graphicsView->repaint();
}
