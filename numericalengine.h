#ifndef NUMERICALENGINE_H
#define NUMERICALENGINE_H

#include <QCoreApplication>
#include <iostream>
#include <limits>


#include <chrono>

#include <gsl/gsl_math.h>
#include <gsl/gsl_poly.h>
#include <zn_poly/zn_poly.h>


#include <QtMath>
#include <QVector>
#include <QList>
#include <QPointF>
#include <QPainterPath>

//#include "KoRTree.h"

constexpr double lowestDouble = std::numeric_limits<double>::min();
constexpr double highestDouble = std::numeric_limits<double>::max();


using namespace std::chrono;

class expression;

class term
    {
    private:
        double coefficient;
        int xIndex;
        int YIndex;

    public:
        friend class expression;
        term ();
        term (double coef, int xIndex, int YIndex);
        ~term ();

        double getCoefficient ();
        int getXIndex ();
        int getYIndex();
        void addCoefficient (double coeff);
        void setCoefficient (double coeff);
        void setXIndex (int xIndex_arg);
        void setYIndex (int YIndex_arg);
        term operator*(term& t2);
        bool operator-(  term &t2);
        bool operator!=(  term &t2);
        bool operator==( const term &t2 );
        bool operator==( term t2 );

    };



class expression
    {
    private:
        QVector<term> express;

    public:
//        expression (std::initializer_list<term> t);
        expression (term t);
        expression ();
        ~expression ();

        expression operator*(expression ex2);
        expression operator*(double multiplying_factor);
        expression operator+(expression ex);
        expression operator-(expression ex);
        bool operator==(expression &ex);
        QVector<double> coeffs ();
        void subtract (expression ex);
        void subtract (term t);
        void add (expression ex);
        void add (term t);

        double evaluate (double x);
        double evaluate2d(double x, double y);
        void validateLeadingCoefficient();
        expression powExpression (int n);
        QVector<term> getExpression();

        enum sortOrder{
            ascending,
            descending
        };

        static bool greaterThan(term &t1, term &t2);
        static bool smallerThan(term &t1, term &t2);

        void sortExpress(sortOrder order);
    };



class cubicBezier
    {
    private:
        QPointF cp0;
        QPointF cp1;
        QPointF cp2;
        QPointF cp3;
        QVector<QPointF> control_points;
        QVector<QVector<expression>> bezierMatrix;

        expression implicit_eq;
        expression parametric_x;
        expression parametric_y;
        expression inversionEquationNum;
        expression inversionEquationDen;

    public:
        friend class KisIntersectionFinder;
        cubicBezier ();
        cubicBezier (QPointF& cp1, QPointF& cp2, QPointF& cp3, QPointF& cp4);
        cubicBezier (QPainterPath::Element curve);

        expression element_entry (int first_pt, int second_pt);
        expression getImplicitEquation();
        expression determinant ();
        void generateParametricEquations ();
        QRectF boundingBox();
        QVector<double> findRoots (cubicBezier& cb2);                                 // QVector<double> findRoots (double arr[10], cubicBezier& cb2);
        expression getParametricX() {return parametric_x;};
        expression getParametricY() {return parametric_y;};
        QVector<QPointF> getControlPoints() {return control_points;};
        double inversionEquationEvaluated(QPointF &p);
        ~cubicBezier ();
    };


class Line {
public:
    Line();
    Line(QPointF p1, QPointF p2);
    ~Line();

    QLineF getQLine();
    expression getImplicitEquation();
    QPointF lineIntersection(Line &l2);
    QVector<double> findRoots(cubicBezier &cb);
    QVector<QPointF> curveIntersection(cubicBezier &cb);
    bool checkIntersection(Line &l2);
    bool checkIntersection(QLineF &l2);
    bool checkIntersection(QPointF &p2);
    expression getParametricX();
    expression getParametricY();

    enum lineTypeEnum {
        Vertical,
        Horizontal,
        Oblique
    };

    bool isVertical();
    bool isHorizontal();
    bool isOblique();
    Line(QLineF line);

private:
    QLineF QtLine;
    double slope;
    double intercept;
    expression implicitEquation;  // of the form:  mx - y + c = 0, where m represents slope and c represents the intercept.
    expression parametrixX;
    expression parametricY;
    lineTypeEnum lineType;
};





#endif // NUMERICALENGINE_H
