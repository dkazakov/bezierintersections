#include "numericalenginetest.h"
#include <QTest>
#include <iostream>

bool compareApprox(double num1, double num2) {
    return (qAbs(num1 - num2) < 1e-8);
}

NumericalEngineTest::NumericalEngineTest(QObject *parent) : QObject(parent)
{

}

void NumericalEngineTest::leadingCoefficientTest() {
    expression ex1a;
    ex1a.add(term(5, 2, 0));
    ex1a.add(term(2, 1, 0));
    ex1a.add(term(1, 0, 0));
    ex1a.subtract(term(5,2,0));
    ex1a.sortExpress(expression::descending);
    ex1a.validateLeadingCoefficient();

    expression ex1b;
    ex1b.add(term(2,1,0));
    ex1b.add(term(1,0,0));
    ex1b.sortExpress(expression::descending);
    ex1b.validateLeadingCoefficient();


    QVERIFY(ex1a == ex1b);

    expression ex2a;

    ex2a.add(term(0,3,0));
    ex2a.add(term(-4,2,0));
    ex2a.add(term(0,1,0));

    ex2a.validateLeadingCoefficient();

    expression ex2b;

    ex2b.add(term(56,3,0));
    ex2b.add(term(-4, 2, 0));
    ex2b.add(term(0,1,0));

    ex2b.validateLeadingCoefficient();

    QVERIFY(!(ex2a == ex2b));



}

void NumericalEngineTest::expressionEvaluateTest() {

    // x = 0, single variable
    expression ex1a;
    ex1a.add(term(90, 2, 0));
    ex1a.add(term(-20, 1, 0));
    ex1a.add(term(1, 0, 0));
    ex1a.subtract(term(5,2,0));

    double sum = ex1a.evaluate(0);

    QVERIFY(sum == 1);

    // x = 2, single variable
    expression ex2a;
    ex2a.add(term(5,2,0));
    ex2a.add(term(0,1,0));
    ex2a.add(term(-10,0,0));

    double sum2 = ex2a.evaluate(2);

    QVERIFY( sum2 == 10);


    // x = 2, single variable but with non-zero y index
    // should give a warning
    expression ex3a;
    ex3a.add(term(5,2,0));
    ex3a.add(term(0,1,0));
    ex3a.add(term(-10,0,2));

    double sum3 = ex3a.evaluate(2);

    QVERIFY( sum3 == 10);

    // x = 2, single variable but with non-zero y index
    // should give a warning
    expression ex4a;
    ex4a.add(term(5,2,0));
    ex4a.add(term(0,1,0));
    ex4a.add(term(-10,0,2));

    double sum4 = ex4a.evaluate2d(2,5);

    QVERIFY( sum4 == -230);

}
//void NumericalEngineTest::additionTest() {
//    expression ex1a;
//    ex1a.add(term(5, 0, 2));
//    ex1a.add(term(-1, 0, 2));

//}
void NumericalEngineTest::multiplicationTest() {

    expression ex1a;

    ex1a.add(term(90, 2, 0));
    ex1a.add(term(-20, 1, 0));
    ex1a.add(term(1, 0, 0));


    expression ex1b;

    ex1b.add(term(5,2,2));
//    ex1b.add(term(0,1,0));
    ex1b.add(term(-10,0,0));


    expression result = ex1a * ex1b;

    expression expectedResult;
    expectedResult.add(term(450,4,2));
    expectedResult.add(term(-900, 2, 0));
    expectedResult.add(term(-100,3,2));
    expectedResult.add(term(200,1,0));
    expectedResult.add(term(5, 2, 2));
    expectedResult.add(term(-10, 0, 0));

    QVERIFY(result == expectedResult);


}

void NumericalEngineTest::gslRootFindingTest() {

    double arr[10] = { 10, -200, 20,0,32.88,4,-8,6,5,10.00001 };
    double z[2 * 10];

    gsl_poly_complex_workspace* w = gsl_poly_complex_workspace_alloc (10);

    gsl_poly_complex_solve (arr, 10, w, z);

    gsl_poly_complex_workspace_free (w);

    QVector<double> realRootComponent;
    QVector<double> imaginaryRootComponent;

    for ( int i = 0; i < 10; i++ ) {
        realRootComponent.push_back (z[2 * i]);
        imaginaryRootComponent.push_back(z[2 * i + 1]);
        }

    QVector<double> expectedRealResult = {1.264781613,1.030798058,1.030798058,-0.2017115816,-0.2017115816,-0.9762505033,-0.9762505033,-1.520706656,0.05025359669,0};
    QVector<double> expectedImaginaryResult = {0,1.005489164,-1.005489164,1.44938063,-1.44938063,1.173438352,-1.173438352,0,0,0};

    std::sort(realRootComponent.begin(), realRootComponent.end());
    std::sort(imaginaryRootComponent.begin(), imaginaryRootComponent.end());
    std::sort(expectedRealResult.begin(), expectedRealResult.end());
    std::sort(expectedImaginaryResult.begin(), expectedImaginaryResult.end());

    bool isEqual = true;

    for (int i = 0; i < 10; i++) {
        if (!compareApprox(realRootComponent.at(i), expectedRealResult.at(i)) ||
                !compareApprox(imaginaryRootComponent.at(i), expectedImaginaryResult.at(i))) {
            isEqual = false;
            break;
        }
    }

    QVERIFY(isEqual);
}

//QTEST_MAIN(NumericalEngineTest);
#include "numericalenginetest.moc"
