#ifndef NUMERICALENGINETEST_H
#define NUMERICALENGINETEST_H

#include <QObject>
#include <QTest>
#include "numericalengine.h"

class NumericalEngineTest : public QObject
{
    Q_OBJECT
public:
    explicit NumericalEngineTest(QObject *parent = nullptr);

private slots:
    
    void leadingCoefficientTest();
    void expressionEvaluateTest();
//    void additionTest();
    void multiplicationTest();

    void gslRootFindingTest();



    
signals:

};

#endif // NUMERICALENGINETEST_H


