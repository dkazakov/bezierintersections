#include <QCoreApplication>
#include <iostream>

#include <chrono>

#include <gsl/gsl_math.h>
#include <gsl/gsl_poly.h>
#include <zn_poly/zn_poly.h>

#include <QtMath>
#include <QVector>
#include <QList>
#include <QPointF>
#include <QPainterPath>

//#include "KoRTree.h"
#include "numericalengine.h"



using namespace std::chrono;

long int factorial (int num)
    {
    long int factorial_num = num;
    if ( num == 0 ) {
        factorial_num = 1;
        }

    else {
        for ( int i = num - 1; i > 0; i-- ) {
            factorial_num *= i;
            }
        }

    return factorial_num;
    }

int combinatorial (int n, int r)
    {
    return ( factorial (n) / ( factorial (r) * factorial (n - r) ) );
    }

class expression;




// class term:
// represents an algebraic term in a polynomial. has 3 data members:
// coef, for the coefficient of a term
// x_ind for the degree of x, and y_ind for the degree of y
// supports changing the values and elementary arithmetic operations like addition, subtraction, and multiplication


term::term (double coef, int x_ind, int y_ind) : coefficient (coef), xIndex (x_ind), YIndex (y_ind)
    {
    }

term::term ()
    {
    }

double term::getCoefficient ()
    {
    return coefficient;
    }

int term::getXIndex () {
    return xIndex;
};

int term::getYIndex () {
    return YIndex;
};

// increments the coefficient by the specified value
void term::addCoefficient (double coeff) {
    this->coefficient += coeff;
};

void term::setCoefficient (double coeff) {
    this->coefficient = coeff;
};

void term::setXIndex (int xIndex_arg) {
    this->xIndex = xIndex_arg;
};

void term::setYIndex (int YIndex_arg) {
    this->YIndex = YIndex_arg;
};

term::~term ()
    {
    }

// multiplies two terms.
// returns a new multiplied term, without modifying the original terms
term term::operator*(term& t2)
    {
    term res;
    res.setCoefficient (( this->coefficient ) * t2.coefficient);
    res.setXIndex (( this->xIndex ) + t2.xIndex);
    res.setYIndex (( this->YIndex ) + t2.YIndex);
    return res;
    }


bool term::operator== (const term &t2) {
    return (coefficient == t2.coefficient && xIndex == t2.xIndex && YIndex == t2.YIndex) ;
}

bool term::operator== (term t2) {
    return (coefficient == t2.coefficient && xIndex == t2.xIndex && YIndex == t2.YIndex) ;
}

//bool term::operator== (const term &t1 term t2) {
//    return (coefficient == t2.coefficient && xIndex == t2.xIndex && YIndex == t2.YIndex) ;
//}

bool term::operator!= (term &t2) {
    return (this->coefficient != t2.coefficient || this->xIndex != t2.xIndex || this->YIndex != t2.YIndex) ;
}

//bool term::operator-(term &t2) {
//    return (this != &t2);
//}













expression::expression ()
    {
        express.clear();
    }


// substitutes the given value for x variable in the expression and returns value of the expression.
// this function ignores the y exponent and assumes the exponent to be zero
double expression::evaluate (double x)
    {
    double result = 0;
    Q_FOREACH ( term i, express ) {
        if (i.YIndex != 0) {
            qWarning("WARNING: equation has two variables, only one is expected");
        }
        result += i.getCoefficient() * pow (x, i.getXIndex());
        }

    return result;

    }


// substitutes the given values for x and y variables in the expression and returns value of the expression.
double expression::evaluate2d (double x, double y)
    {
    double result = 0;
    Q_FOREACH( term i , express ) {
        result += i.getCoefficient() * pow (x, i.getXIndex()) * pow(y, i.getYIndex());
        }

    return result;

    }

// checks if the leading term has coefficient with value zero, and removes it until the leading term has a non-zero coefficient
// works only for polynomials in one variable.
void expression::validateLeadingCoefficient() {
    sortExpress(descending);
    while ( express.first().coefficient == 0 && express.size() >= 0) {
        express.removeFirst();
    }
}

// exponentiates the expression by multiplying itself n times
expression expression::powExpression (int n)
    {
    expression result;
    expression result2;
    result.add (*this );

    for ( int i = 1; i < n; i++ ) {
        result2 = result * ( *this );
        result = result2;
        }


    return result;
    }


// returns the QVector containing all the terms in the expression
QVector<term> expression::getExpression(){
    return this->express;
}




expression expression::operator+(expression ex)
    {
    expression result;

    Q_FOREACH ( term i , express ) {
        result.add (i);
        }
    result.add (ex);

    return result;
    }

expression expression::operator-(expression ex)
    {
    expression result;

    result.add (*this);
    result.subtract (ex);

    return result;
    }

bool expression::operator==(expression &ex ) {

    if (this->express.size() != ex.express.size()) {
        return false;
    }

    for (int i = 0; i < express.size(); i++) {
        if (express[i] != ex.express[i]) {
            return false;
        }
    }

    return true;
}

void expression::add (term t)
    {
    if ( express.size () == 0 ) {
        express.push_back (t);
        return;
        }
    for ( int i = 0; i < this->express.size ();i++ ) {
        term curr = express.at(i);

        if ( ( curr.getXIndex () == t.getXIndex () ) && ( curr.getYIndex () == t.getYIndex () ) ) {
            (express[i]).setCoefficient (curr.getCoefficient() + t.getCoefficient());
            return;
            }
        }
    express.push_back (t);

    }

void expression::add (expression ex)
    {
    Q_FOREACH ( term i , ex.express ) {
        this->add (i);
        }
    }

void expression::subtract (term t)
    {

    if ( express.size () == 0 ) {
        // empty expression, expression remains same
        return;
        }
    for ( int i = 0; i < this->express.size();i++ ) {
        term curr = express.at (i);
        if ( ( curr.getXIndex () == t.getXIndex () ) && ( curr.getYIndex () == t.getYIndex () ) ) {
            express[i].setCoefficient (curr.getCoefficient() - t.getCoefficient());
            return;
            }
        }
    t.setCoefficient (-t.getCoefficient ());
    this->express.push_back (t);
    }

void expression::subtract (expression ex)
    {

    Q_FOREACH ( term i , ex.express ) {
        this->subtract (i);
        }
    }

expression expression::operator*(expression ex2)
    {

    expression result;
    Q_FOREACH( term i , this->express ) {
        double i_coef = i.getCoefficient ();
        int i_x = i.getXIndex ();
        int i_y = i.getYIndex ();
        Q_FOREACH ( term j , ex2.express ) {
            double j_coef = j.getCoefficient ();
            int j_x = j.getXIndex ();
            int j_y = j.getYIndex ();

            term resultant_term;
            resultant_term.setCoefficient (i_coef * j_coef); //i.getCoefficient() * j.getCoefficient()
            resultant_term.setXIndex (i_x + j_x); //i.getXIndex() + j.getXIndex()
            resultant_term.setYIndex (i_y + j_y); //i.getYIndex() + j.getYIndex()
            result.add (resultant_term);
            }
        }


    return result;
    }

bool expression::greaterThan(term &t1, term &t2) {
    // compares the x index of two terms and returns true if first is smaller than the other
    return t1.xIndex < t2.xIndex;
}

bool expression::smallerThan(term &t1, term &t2) {
    // compares the x index of two terms and returns true if first is greater than the other
    return t1.xIndex > t2.xIndex;
}

void expression::sortExpress(expression::sortOrder order = ascending) {
    // sorts the expression in ascending or descending order with respect to index of the first variable (x or t)
    std::sort (this->express.begin(), this->express.end(), (order == ascending ? greaterThan : smallerThan));
}

expression expression::operator*(double multiplying_factor)
    {
    // multiplies an expression by a constant factor
    // returns a new expression
    expression result;
    Q_FOREACH ( term i , this->express ) {
        term resterm (i.getCoefficient () * multiplying_factor, i.getXIndex (), i.getYIndex ());
        result.add (resterm);
        }

    return result;
    }

QVector<double> expression::coeffs ()
    {
    QVector<double> result;
    Q_FOREACH ( term i , express ) {
        result.push_back (i.getCoefficient ());
        }
    return result;
    }

expression::~expression ()
    {
    }













cubicBezier::cubicBezier() {}
cubicBezier::~cubicBezier() {}

cubicBezier::cubicBezier (QPointF& cp_0, QPointF& cp_1, QPointF& cp_2, QPointF& cp_3) : cp0 (cp_0), cp1 (cp_1), cp2 (cp_2), cp3 (cp_3)
    {
    control_points.append(QVector<QPointF>{cp0, cp1, cp2, cp3});

    expression ex;
    QVector<expression> row{3, ex};

    for(int i = 0; i < 3; i++) {
        bezierMatrix.push_back(row);
    }

    generateParametricEquations();
    implicit_eq = getImplicitEquation();

    }

cubicBezier::cubicBezier(QPainterPath::Element curve) {
    cp0 =curve;

}

void cubicBezier::generateParametricEquations()
    {

    term term_t3 (cp3.x() - 3 * cp2.x () + 3 * cp1.x () - cp0.x (), 3, 0);
    term term_t2 (3 * cp2.x () - 6 * cp1.x () + 3 * cp0.x (), 2, 0);
    term term_t1 (3 * cp1.x () - 3 * cp0.x (), 1, 0);
    term term_t0 (cp0.x (), 0, 0);

    term term_t3_y (cp3.y() - 3 * cp2.y () + 3 * cp1.y () - cp0.y (), 3, 0);
    term term_t2_y (3 * cp2.y () - 6 * cp1.y () + 3 * cp0.y (), 2, 0);
    term term_t1_y (3 * cp1.y () - 3 * cp0.y (),1, 0);
    term term_t0_y (cp0.y (), 0, 0);

    parametric_x.add (term_t3);
    parametric_x.add (term_t2);
    parametric_x.add (term_t1);
    parametric_x.add (term_t0);

    parametric_y.add (term_t3_y);
    parametric_y.add (term_t2_y);
    parametric_y.add (term_t1_y);
    parametric_y.add (term_t0_y);

}

expression cubicBezier::element_entry(int first_pt, int second_pt)
    {
    // generates the element entry needed for implicitization matrix
 //   std::cout<<"p1: "<<first_pt<< "p2: "<< second_pt <<std::endl;

    // where
    // l ij (x, y) = 3Ci  *  3Cj *  | x    y    1 |
    //                              | xi   yi   1 |
    //                              | xj   yj   1 |
    QPointF p1 (control_points[first_pt]);
    QPointF p2 (control_points[second_pt]);


    term x_term{ p1.y () - p2.y (), 1, 0 };
    term y_term{ p2.x () - p1.x (), 0, 1 };
    term const_term{ ( p1.x () * p2.y () ) - ( p1.y () * p2.x () ), 0, 0 };
    expression entry;
    entry.add (x_term);
    entry.add (y_term);
    entry.add (const_term);

    int combinatorial_factor = ( combinatorial (3, first_pt) * combinatorial (3, second_pt) );

    entry = entry * combinatorial_factor;

    return entry;
    }



expression cubicBezier::getImplicitEquation()
    {
    expression result;
    //   and a degree 3 Bézier curve can be implicitized
    // f (x, y) =  | l 32 (x, y)        l 31 (x, y)             l 30 (x, y) |
    //             | l 31 (x, y)   l 30 (x, y) + l 21 (x, y)    l 20 (x, y) |
    //             | l 30 (x, y)        l 20 (x, y)             l 10 (x, y) |

    // where
    // l ij (x, y) = 3Ci  *  3Cj *  | x    y    1 |
    //                              | xi   yi   1 |
    //                              | xj   yj   1 |

    bezierMatrix[0][0] = element_entry (3, 2);
    bezierMatrix[0][1] = element_entry (3, 1);
    bezierMatrix[0][2] = element_entry (3, 0);

    bezierMatrix[1][0] = element_entry (3, 1);
    bezierMatrix[1][1] = element_entry (3, 0) + ( element_entry (2, 1) );
    bezierMatrix[1][2] = element_entry (2, 0);

    bezierMatrix[2][0] = element_entry (3, 0);
    bezierMatrix[2][1] = element_entry (2, 0);
    bezierMatrix[2][2] = element_entry (1, 0);

    expression cofactor_00 = bezierMatrix[0][0] * ( ( bezierMatrix[1][1] * bezierMatrix[2][2] ) - ( bezierMatrix[1][2] * bezierMatrix[2][1] ) );

    expression cofactor_01 = bezierMatrix[0][1] * ( ( bezierMatrix[1][0] * bezierMatrix[2][2] ) - ( bezierMatrix[1][2] * bezierMatrix[2][0] ) ) * -1;

    expression cofactor_02 = bezierMatrix[0][2] * ( ( bezierMatrix[1][0] * bezierMatrix[2][1] ) - ( bezierMatrix[1][1] * bezierMatrix[2][0] ) );

    result.add (cofactor_00);
    result.add(cofactor_01);//result.subtract (cofactor_01);
    result.add (cofactor_02);

//    std::cout << "implicit eqns generated within the function" << std::endl;

//    expression main_result;

//    Q_FOREACH( auto i, result.getExpression()) {
//        if (i.getCoefficient() == 0) {
//            continue;
//        }
//        main_result.add(i);
//        std::cout.precision(25);
//    }


    return result;
    }

QRectF cubicBezier::boundingBox() {

    double xMin = control_points[0].rx();
    double xMax = control_points[0].rx();

    double yMin = control_points[0].ry();
    double yMax = control_points[0].ry();

    Q_FOREACH(QPointF pt, control_points) {
        if (pt.rx() < xMin) {
            xMin = pt.rx();
        }

        if (pt.rx() > xMax) {
            xMax = pt.rx();
        }

        if (pt.ry() < yMin) {
            yMin = pt.ry();
        }

        if (pt.ry() > yMax) {
            yMax = pt.ry();
        }
    }

    return QRectF( QPointF(xMax, yMax), QPointF(xMin,yMin));
}

double cubicBezier::inversionEquationEvaluated(QPointF &p) {
    // l b (x, y) − l a (x, y)

//    l a (x, y) = c 1 l 31 (x, y) + c 2 [l 30 (x, y) + l 21 (x, y)] + l 20 (x, y)
//    l b (x, y) = c 1 l 30 (x, y) + c 2 l 20 (x, y) + l 10 (x, y)

    if(inversionEquationDen.getExpression().size() == 0 && inversionEquationNum.getExpression().size() == 0 ) {
        double c1Num = cp0.x() * (cp1.y() - cp3.y()) - cp0.y() * (cp1.x() - cp3.x())  + cp1.x() * cp3.y() - cp1.y() * cp3.x();
        double c1Den = cp1.x() * (cp2.y() - cp3.y()) - cp1.y() * (cp2.x() - cp3.x())  + cp2.x() * cp3.y() - cp3.x() * cp2.y();
        double c1 = c1Num / (3 * c1Den);

        double c2Num = cp0.x() * (cp2.y() - cp3.y()) - cp0.y() * (cp2.x() - cp3.x())  + cp2.x() * cp3.y() - cp2.y() * cp3.x();
        double c2Den = c1Den;
        double c2 = -(c2Num / (3 * c2Den) );

        expression la = bezierMatrix[0][1] * c1 + bezierMatrix[1][1] * c2 + bezierMatrix[1][2];
        expression lb = bezierMatrix[0][2] * c1 + bezierMatrix[1][2] * c2 + bezierMatrix[2][2];

        inversionEquationNum = lb;
        inversionEquationDen = lb - la;
    }

    double t = inversionEquationNum.evaluate2d(p.x(), p.y()) / inversionEquationDen.evaluate2d(p.x(), p.y());

    return t;
}

QVector<double> cubicBezier::findRoots(cubicBezier& cb2)
    {
    expression impl = cb2.getImplicitEquation();
    expression result;


//    std::cout << "c1 para x:" << std::endl;
//    Q_FOREACH(term t, parametric_x.getExpression()) {
//        std::cout << t.getCoefficient() << "x^" << t.getXIndex() << "y^" << t.getYIndex() << "   " ;
//    }

//    std::cout << "\nc1 para y:" << std::endl;
//    Q_FOREACH(term t, parametric_y.getExpression()) {
//        std::cout << t.getCoefficient() << "x^" << t.getXIndex() << "y^" << t.getYIndex() << "   " ;
//    }

    Q_FOREACH ( auto i , impl.getExpression() ) {
//        std::cout << "implicit individual expression:   ";
//        std::cout << i.getCoefficient() << "x^" << i.getXIndex() << "y^" << i.getYIndex() << "   \n" ;
        expression individual_result;
        term unity{ 1, 0, 0 };

//        if (i.getCoefficient() == 0) {
//            continue;
//        }

        expression e1, e2;
        if ( i.getXIndex() != 0 ) {
            e1.add(parametric_x.powExpression (i.getXIndex()));
            }
        else {
            e1.add (unity);
            }

        if ( i.getYIndex() != 0 ) {
            e2.add(parametric_y.powExpression (i.getYIndex()));
            }
        else {
            e2.add (unity);
            }

        individual_result = ( e1 * e2 ) * i.getCoefficient();

        result.add(individual_result);
        }

    result.validateLeadingCoefficient();



    result.sortExpress();

    int numTerms = result.getExpression().size();

    double arr[numTerms];

    for ( int j = 0; j < numTerms; j++ ) {

        arr[j] = (result.getExpression()[j]).getCoefficient();
        }

    double z[2 * numTerms];

    gsl_poly_complex_workspace* w = gsl_poly_complex_workspace_alloc (numTerms);

    gsl_poly_complex_solve (arr, numTerms, w, z);

    gsl_poly_complex_workspace_free (w);

    QVector<double> t_roots;

    for ( int i = 0; i < numTerms; i++ ) {
//        std::cout <<"root "<< i << ": "<< z[2*i]<<" "  << z[2*i +1] << std::endl;  // << sign.toStdString()
        if ( z[2 * i] > 0 && z[2 * i] < 1 && z[2 * i + 1] == 0 ) {

            t_roots.push_back (z[2 * i]);
            }

        }

//    std::cout << " intersection points in findroots()" << std::endl;
//    Q_FOREACH ( auto i , t_roots ) {

//        std::cout << "x: " << this->parametric_x.evaluate (i) << " y: " << this->parametric_y.evaluate (i) << std::endl;
//        }

    return t_roots;
    }



Line::Line(QPointF p1, QPointF p2) :  QtLine(p1, p2){

 ;
    // form:  mx - y + c = 0, where m represents slope and c represents the intercept.



    if ( QtLine.y1() == QtLine.y2()) { // qAbs(qTan(qDegreesToRadians(QtLine.angle()))) > highestDouble ||
        // line is of the form y = a, i.e 0x + y - a = 0
        lineType = Horizontal;
//        slope = 1;
        intercept = - QtLine.y1();

//        implicitEquation.add(term(0, 1, 0));
        implicitEquation.add(term(1,0,1));
        implicitEquation.add(term(intercept ,0,0));

    }

    else if (QtLine.x1() == QtLine.x2()) { // qAbs(qTan(qDegreesToRadians(QtLine.angle()))) < lowestDouble ||
        // line is of the form x = a, i.e x + 0y - a = 0
        lineType = Vertical;
        intercept = - QtLine.x1();

        implicitEquation.add(term(1, 1, 0));
//        implicitEquation.add(term(1,0,1));
        implicitEquation.add(term(intercept ,0,0));
    }

    else {
        lineType = Oblique;
        slope = (QtLine.y2() - QtLine.y1()) / (QtLine.x2() - QtLine.x1());
        intercept = QtLine.y1() - (slope * QtLine.x1());

        implicitEquation.add(term(slope, 1, 0));
        implicitEquation.add(term(-1,0,1));
        implicitEquation.add(term(intercept,0,0));

        parametrixX.add(term(QtLine.x1(), 0, 0));
        parametrixX.add(term(QtLine.dx(),1,0));

        parametricY.add(term(QtLine.y1(), 0, 0));
        parametricY.add(term(QtLine.dy(),1,0));
    }
}

Line::Line() {

}

Line::~Line() {

}


Line::Line(QLineF line) : QtLine(line){

    // form:  mx - y + c = 0, where m represents slope and c represents the intercept.



    if (line.y1() == line.y2()) {
        // line is of the form y = a, i.e 0x + y - a = 0
        lineType = Horizontal;
//        slope = 1;
        intercept = - line.y1();

//        implicitEquation.add(term(0, 1, 0));
        implicitEquation.add(term(1,0,1));
        implicitEquation.add(term(intercept ,0,0));

    }

    else if (line.x1() == line.x2()) {
        // line is of the form x = a, i.e x + 0y - a = 0
        lineType = Vertical;
        intercept = - line.x1();

        implicitEquation.add(term(1, 1, 0));
//        implicitEquation.add(term(1,0,1));
        implicitEquation.add(term(intercept ,0,0));
    }

    else {
        lineType = Oblique;
        slope = (line.y2() - line.y1()) / (line.x2() - line.x1());
        intercept = line.y1() - (slope * line.x1());

        implicitEquation.add(term(slope, 1, 0));
        implicitEquation.add(term(-1,0,1));
        implicitEquation.add(term(intercept,0,0));

        parametrixX.add(term(line.x1(), 0, 0));
        parametrixX.add(term(line.dx(),1,0));

        parametricY.add(term(line.y1(), 0, 0));
        parametricY.add(term(line.dy(),1,0));
    }

}


QLineF Line::getQLine() {
    return QtLine;
}

expression Line::getImplicitEquation() {
    return implicitEquation;
}

QVector<double> Line::findRoots(cubicBezier &cb) {
    // finds intersection points of a line with a curve.
    // this function assumes the line isn't verical or horizontal, and the line does intersect with the bounding box of curve

    expression implicitExpression = this->getImplicitEquation();
    expression result;

    if (isVertical()) {
        // all points on the line segment will have the same x co-ordinate, hence we can obtain the parameter value t by substituting the
        // value of intercept in the parametric equation of x of the Bezier curve.

//        expression parametricXCopy = cb.getParametricX() + term(intercept, 0, 0);
        result.add(cb.getParametricX());
        result.add(term(intercept, 0, 0));

//        std::cout << "\n\n\nis Vertical, X should be used" << std::endl;
    }

    else if (isHorizontal()) {

//        expression parametricXCopy = cb.getParametricX() + term(intercept, 0, 0);
        result.add(cb.getParametricY());
        result.add(term(intercept, 0, 0));

//        std::cout << "\n\n\nis Horizontal, Y should be used" << std::endl;
    }

    else if (isOblique()) {

        Q_FOREACH ( auto i , implicitExpression.getExpression() ) {
            expression individual_result;
            term unity{ 1, 0, 0 };


            expression e1, e2;
            if ( i.getXIndex() != 0 ) {
                e1.add(cb.getParametricX());
                }
            else {
                e1.add (unity);
                }

            if ( i.getYIndex() != 0 ) {
                e2.add(cb.getParametricY());
                }
            else {
                e2.add (unity);
                }

            individual_result = ( e1 * e2 ) * i.getCoefficient();

            result.add(individual_result);
            }
    }




    result.sortExpress(expression::descending);

    result.validateLeadingCoefficient();

    result.sortExpress(expression::ascending);

//    std::cout << "X" << std::endl;

//    Q_FOREACH(term t, cb.getParametricX().getExpression()) {
//        std::cout << t.getXIndex() << std::endl;
//    }

//    std::cout << "Y" << std::endl;

//    Q_FOREACH(term t, cb.getParametricY().getExpression()) {
//        std::cout << t.getXIndex() << std::endl;
//    }

//    Q_FOREACH(term t, result.getExpression()) {
//        std::cout << t.getXIndex() << std::endl;
//    }

    int numTerms = result.getExpression().size();

    double arr[numTerms];

    for ( int j = 0; j < numTerms; j++ ) {

        arr[j] = (result.getExpression()[j]).getCoefficient();
        }

    double z[2 * numTerms];

    gsl_poly_complex_workspace* w = gsl_poly_complex_workspace_alloc (numTerms);

    gsl_poly_complex_solve (arr, numTerms, w, z);

    gsl_poly_complex_workspace_free (w);

    QVector<double> t_roots;

    for ( int i = 0; i < numTerms; i++ ) {
        if ( z[2 * i] > 0 && z[2 * i] < 1 && z[2 * i + 1] == 0 ) {

            t_roots.push_back (z[2 * i]);
            }

        }


//    std::cout << " intersection points in findroots(line,curve)" << std::endl;
//    Q_FOREACH ( auto i , t_roots ) {

//        std::cout << "x: " << cb.getParametricX().evaluate (i) << " y: " << cb.getParametricY().evaluate (i) << std::endl;
//        }

    return t_roots;

}



bool Line::checkIntersection(Line &l2) {

    QPointF* intersection;
    // intersect() is deprecated, however the newer function was introduced in version 5.14
    QLineF::IntersectType intersect =  this->getQLine().intersect(l2.getQLine(),intersection);

    return (intersect == QLineF::BoundedIntersection) ? true : false;
}

bool Line::checkIntersection(QLineF &l2) {

    QPointF* intersection;
    // intersect() is deprecated, however the newer function was introduced in version 5.14
    QLineF::IntersectType intersect =  l2.intersect(this->getQLine(),intersection);

    QLineF::IntersectionType intersect2 =  l2.intersects(this->getQLine(),intersection);
    return (intersect == QLineF::IntersectType::BoundedIntersection) ? true : false;
}

bool Line::checkIntersection(QPointF &PointToCheck) {

    double resultant = this->getImplicitEquation().evaluate2d(PointToCheck.x(), PointToCheck.y());
//    std::cout << "pt line int val" <<resultant << std::endl;

    // as we're dealing with floating points, some error is generated. Hence, resultant is not equal to zero even though the point lies on the line.
    if (abs(resultant) > 0.0000001) {
        return false;
    }

    // the point must lie on the line segment, hence its co-ordinates must be in between the end points of the line segment
    if (qMin(QtLine.p1().x(), QtLine.p2().x()) < PointToCheck.x() + 0.000001 && qMax(QtLine.p1().x(), QtLine.p2().x()) > PointToCheck.x() - 0.000001
        && qMin(QtLine.p1().y(), QtLine.p2().y()) < PointToCheck.y() + 0.000001 && qMax(QtLine.p1().y(), QtLine.p2().y()) > PointToCheck.y() - 0.000001) {

        return true;
    }

    else {
        return false;
    }
}

expression Line::getParametricX() {
    return this->parametrixX;
}

expression Line::getParametricY() {
    return this->parametricY;
}

bool Line::isVertical() {
    return lineType == Vertical ? true : false;
}

bool Line::isHorizontal() {
    return lineType == Horizontal ? true : false;
}

bool Line::isOblique() {
    return lineType == Oblique ? true : false;
}












int main33 ()
    {
    auto start = high_resolution_clock::now ();

    ;

    QPointF cp0(0,0); //(4, 1);
    QPointF cp1(0,2); //(8, 4);
    QPointF cp2(2,2); //(6, 7);
    QPointF cp3(2,0); //(2, 6);


    cubicBezier cb1{ cp0, cp1, cp2, cp3 }; // cp0, cp1, cp2, cp3    cp3, cp2, cp1, cp0

    QPointF cba(2,0); //{ 3,3 };
    QPointF cbb(2,2); //{ 8,1 };
    QPointF cbc(4, 2); //{ 12,2 };
    QPointF cbd(4, 0); //{ 11,5 };

    cubicBezier cb2{ cba, cbb, cbc, cbd }; //  cba, cbb, cbc, cbd          cbd,cbc,cbb,cba


//    KisIntersectionFinder KIF;
//    QVector<QPointF> points = KIF.intersectionPoint(cb1, cb2);

//    if (!points.size()) {
//        std::cout<< "they dont intersect!\n";
//    }
//    else {
//        Q_FOREACH(QPointF pt, points) {
//            std::cout << "in function of KisIntersectionFinder\n";
//            std::cout << "x: "<<pt.rx()<<"   y: "<<pt.ry()<<"\n";
//        }
//    }


    auto stop = high_resolution_clock::now ();

    auto duration = duration_cast<microseconds>( stop - start );

    std::cout << "total time: " << duration.count () << " microseconds" << std::endl;

    QVector<double> arr {0,1,2,3,4,5};
    std::cout<< "*-*-*-*-*-*-*-*-*-*-*\n";
    std::cout<< arr[0];

    return 0;



    }
