#include "kisintersectionfinder.h"
#include "bezier.h"
#include "databuffer.h"
#include "painterpath.h"
#include "painterpath_p.h"
#include "numericalengine.h"
#include "pathclipper.h"
#include "vectorpath.h"

// helper functions

static inline bool fuzzyIsNull(qreal d)
{
    if (sizeof(qreal) == sizeof(double))
        return qAbs(d) <= 1e-12;
    else
        return qAbs(d) <= 1e-5f;
}

static inline bool comparePoints(const QPointF &a, const QPointF &b)
{
    // the epsilon in fuzzyIsNull is far too small for our use

//    return fuzzyIsNull(a.x() - b.x())
//           && fuzzyIsNull(a.y() - b.y());

    return qAbs(a.x() - b.x()) < 1e-6 && qAbs(a.y() - b.y()) < 1e-6;
}

// returns dot product of two point vectors
static qreal dot(const QPointF &a, const QPointF &b)
{
    return a.x() * b.x() + a.y() * b.y();
}

KisIntersectionFinder::KisIntersectionFinder(MyPainterPath subject, MyPainterPath clip) {
    // converts MyPainterPath (same as QPainterPath) to a sequence of Line and cubicBezier objects

    int segId = 0;
    QPointF currPoint(0,0);
    QPointF lastPoint(0,0);
    for (int i = 0; i < subject.elementCount(); i++) {
        MyPainterPath::Element e = subject.elementAt(i);

        // ignore moveTo elements as there is no segment for intersection
        if (e.isMoveTo()) {
            lastPoint = e;

        }

        else if (e.isLineTo()) {
            currPoint = QPointF(e.x, e.y);
            buildingBlock lineEle(segId,LineToElement,Line(lastPoint, currPoint));
            subjectShape.push_back(lineEle);
            lastPoint = currPoint;
        }

        // Qt elavates quadratic Bezier curves to cubic curves inside QPainterPath, hence only cubic curves with four control points are expected
        else if (e.isCurveTo()) {

            QPointF cp2(e);
            QPointF cp3(subject.elementAt(i+1));
            QPointF cp4(subject.elementAt(i+2));
            cubicBezier cubic(lastPoint, cp2, cp3, cp4);

            buildingBlock curveEle(segId,CurveToElement,cubic);
            subjectShape.push_back(curveEle);
            lastPoint = cp4;
            i += 2;
        }

        segId++;
    }

    // reset counter variables and repeat procedure for clip shape
    lastPoint = QPointF(0, 0);
    currPoint = QPointF(0, 0);
    segId = 0;

    for (int i = 0; i < clip.elementCount(); i++) {
        MyPainterPath::Element e = clip.elementAt(i);


        if (e.isMoveTo()) {
            lastPoint = e;
            //continue;
        }
        else if (e.isLineTo()) {
            currPoint = QPointF(e.x, e.y);
            buildingBlock lineEle(segId,LineToElement,Line(lastPoint, currPoint));
            clipShape.push_back(lineEle);
            lastPoint = currPoint;
        }
        else if (e.isCurveTo()) {

            QPointF cp2(e);
            QPointF cp3(clip.elementAt(i+1));
            QPointF cp4(clip.elementAt(i+2));
            cubicBezier cubic(lastPoint, cp2, cp3, cp4);
            i += 2;
            buildingBlock curveEle(segId,CurveToElement,cubic);
            clipShape.push_back(curveEle);
            lastPoint = cp4;

        }

        segId++;
    }

}


// Checks if two line segments intersect. Same as QPathClipper::linesIntersect()
bool KisIntersectionFinder::linesIntersect(const QLineF &a, const QLineF &b) const
{
    const QPointF p1 = a.p1();
    const QPointF p2 = a.p2();

    const QPointF q1 = b.p1();
    const QPointF q2 = b.p2();

    if (comparePoints(p1, p2) || comparePoints(q1, q2))
        return false;

    const bool p1_equals_q1 = comparePoints(p1, q1);
    const bool p2_equals_q2 = comparePoints(p2, q2);

    if (p1_equals_q1 && p2_equals_q2)
        return true;

    const bool p1_equals_q2 = comparePoints(p1, q2);
    const bool p2_equals_q1 = comparePoints(p2, q1);

    if (p1_equals_q2 && p2_equals_q1)
        return true;

    const QPointF pDelta = p2 - p1;
    const QPointF qDelta = q2 - q1;

    const qreal par = pDelta.x() * qDelta.y() - pDelta.y() * qDelta.x();

    if (qFuzzyIsNull(par)) {
        const QPointF normal(-pDelta.y(), pDelta.x());

        // coinciding?
        if (qFuzzyIsNull(dot(normal, q1 - p1))) {
            const qreal dp = dot(pDelta, pDelta);

            const qreal tq1 = dot(pDelta, q1 - p1);
            const qreal tq2 = dot(pDelta, q2 - p1);

            if ((tq1 > 0 && tq1 < dp) || (tq2 > 0 && tq2 < dp))
                return true;

            const qreal dq = dot(qDelta, qDelta);

            const qreal tp1 = dot(qDelta, p1 - q1);
            const qreal tp2 = dot(qDelta, p2 - q1);

            if ((tp1 > 0 && tp1 < dq) || (tp2 > 0 && tp2 < dq))
                return true;
        }

        return false;
    }

    const qreal invPar = 1 / par;

    const qreal tp = (qDelta.y() * (q1.x() - p1.x()) -
                      qDelta.x() * (q1.y() - p1.y())) * invPar;

    if (tp < 0 || tp > 1)
        return false;

    const qreal tq = (pDelta.y() * (q1.x() - p1.x()) -
                      pDelta.x() * (q1.y() - p1.y())) * invPar;

    return tq >= 0 && tq <= 1;
}



// Finds intersection points between two cubic Bezier curves. returns a QVector of the intersection points represented by QPointF class.
QVector<QPointF> KisIntersectionFinder::intersectionPoint(cubicBezier c1, cubicBezier c2){
    QVector<QPointF> result;

//        tree.insert(c1.boundingBox(), c1);
//        tree.insert(c2.boundingBox(), c2);

// tree.intersects(c2.boundingBox()).size() == 0


        if (!c1.boundingBox().intersects(c2.boundingBox())) {
            // if the bounding boxes don't intersect, the curves surely will not intersect
            if (!c1.boundingBox().contains(c2.boundingBox()) && !c2.boundingBox().contains(c1.boundingBox())){
                return result;

            }
        }
//        c2.generateParametricEquations();
//        c1.generateParametricEquations();
    QVector<double> roots = c1.findRoots(c2);

    QPointF ptOfIntersection;

    Q_FOREACH(double root, roots) {
        ptOfIntersection.setX(c1.parametric_x.evaluate(root));
        ptOfIntersection.setY(c1.parametric_y.evaluate(root));

        double supposedParameter = c2.inversionEquationEvaluated(ptOfIntersection);
        if (supposedParameter <= 1 && supposedParameter >= 0) {
            // the parameter lies within the range, hence it lies on the curve bounded by the first and fourth end points

            if (!comparePoints(c1.cp0, ptOfIntersection) && !comparePoints(c1.cp3, ptOfIntersection)
                    && !comparePoints(ptOfIntersection, c2.cp0) && !comparePoints(ptOfIntersection, c2.cp3)) {
                // if the intersection point lies too close to the end points of both curves, reject it
                result.push_back(ptOfIntersection);
            }
        }
    }

    return result;

}


QVector<QPointF> KisIntersectionFinder::intersectionPoint(Line l1, cubicBezier c2) {
    QVector<QPointF> result;

    QRectF curveBoundingBox = c2.boundingBox();
    QLineF qLine = l1.getQLine();

    QVector<double> roots;

    if (curveBoundingBox.contains(qLine.p1()) || curveBoundingBox.contains(qLine.p2())) {
        // at least one of the end points of the line lies in the bounding box, hence proceed to calculate intersection
        roots = l1.findRoots(c2);
    }

    else {
        QLineF topEdge{curveBoundingBox.topLeft(), curveBoundingBox.topRight()};
        QLineF rightEdge{curveBoundingBox.topRight(), curveBoundingBox.bottomRight()};
        QLineF bottomEdge{curveBoundingBox.bottomRight(), curveBoundingBox.bottomLeft()};
        QLineF leftEdge{curveBoundingBox.bottomLeft(), curveBoundingBox.topLeft()};

            if (linesIntersect(qLine,topEdge) || linesIntersect(qLine,rightEdge) || linesIntersect(qLine,bottomEdge) || linesIntersect(qLine,leftEdge)) {
            // line intersects at least one side of the curve, proceed to calculate intersection
            roots = l1.findRoots(c2);
        }
        else {
            // line does not intersect bounding box nor lies within it, so it surely doesn't intersect the curve
//                std::cout << "line does not intersect bounding box nor lies within it, so it surely doesn't intersect the curve" << std::endl;
            return result;
        }
    }


    if(roots.isEmpty()) {
        // line does not intersect the curve
//        std::cout << "roots is empty" << std::endl;
        return result;
    }

    Q_FOREACH (double root, roots) {
//        std::cout << "                                          root: " << root <<std::endl;
        QPointF currIntersectionPoint{c2.getParametricX().evaluate(root) ,  c2.getParametricY().evaluate(root)};
//        std::cout << "point checking  " << ++numzzzz2 <<std::endl;
        if (l1.checkIntersection(currIntersectionPoint)) {
//            std::cout << "point appended! " << ++numzzzz <<std::endl;

            if (!comparePoints(currIntersectionPoint, c2.cp0) && !comparePoints(currIntersectionPoint, c2.cp3)
                && !comparePoints(currIntersectionPoint, qLine.p1()) && !comparePoints(currIntersectionPoint, qLine.p2())) {

                // if the intersection point lies too close to the end points of both curves, reject it
                result.push_back(currIntersectionPoint);
            }
        }

    }
    return result;
}

struct QIntersection
{
    qreal alphaA;
    qreal alphaB;

    QPointF pos;
};


QVector<QPointF> KisIntersectionFinder::intersectionPoint(Line l1,Line l2) {
    QVector<QPointF> intersections;

    QLineF a = l1.getQLine();
    QLineF b = l2.getQLine();

    const QPointF p1 = a.p1();
    const QPointF p2 = a.p2();

    const QPointF q1 = b.p1();
    const QPointF q2 = b.p2();

    if (comparePoints(p1, p2) || comparePoints(q1, q2))
        return intersections;

    const bool p1_equals_q1 = comparePoints(p1, q1);
    const bool p2_equals_q2 = comparePoints(p2, q2);

    if (p1_equals_q1 && p2_equals_q2)
        return intersections;

    const bool p1_equals_q2 = comparePoints(p1, q2);
    const bool p2_equals_q1 = comparePoints(p2, q1);

    if (p1_equals_q2 && p2_equals_q1)
        return intersections;

    const QPointF pDelta = p2 - p1;
    const QPointF qDelta = q2 - q1;

    const qreal par = pDelta.x() * qDelta.y() - pDelta.y() * qDelta.x();

    if (qFuzzyIsNull(par)) {
        const QPointF normal(-pDelta.y(), pDelta.x());

        // coinciding?
        if (qFuzzyIsNull(dot(normal, q1 - p1))) {
            const qreal invDp = 1 / dot(pDelta, pDelta);

            const qreal tq1 = dot(pDelta, q1 - p1) * invDp;
            const qreal tq2 = dot(pDelta, q2 - p1) * invDp;

            if (tq1 > 0 && tq1 < 1) {
                QIntersection intersection;
                intersection.alphaA = tq1;
                intersection.alphaB = 0;
                intersection.pos = q1;

                intersections.push_back(q1);
            }

            if (tq2 > 0 && tq2 < 1) {
                QIntersection intersection;
                intersection.alphaA = tq2;
                intersection.alphaB = 1;
                intersection.pos = q2;

                intersections.push_back(q2);
            }

            const qreal invDq = 1 / dot(qDelta, qDelta);

            const qreal tp1 = dot(qDelta, p1 - q1) * invDq;
            const qreal tp2 = dot(qDelta, p2 - q1) * invDq;

            if (tp1 > 0 && tp1 < 1) {
                QIntersection intersection;
                intersection.alphaA = 0;
                intersection.alphaB = tp1;
                intersection.pos = p1;

                intersections.push_back(p1);
            }

            if (tp2 > 0 && tp2 < 1) {
                QIntersection intersection;
                intersection.alphaA = 1;
                intersection.alphaB = tp2;
                intersection.pos = p2;
                intersections.push_back(p2);
            }
        }

        return intersections;
    }

    // if the lines are not parallel and share a common end point, then they
    // don't intersect
    if (p1_equals_q1 || p1_equals_q2 || p2_equals_q1 || p2_equals_q2)
        return intersections;


    const qreal tp = (qDelta.y() * (q1.x() - p1.x()) -
                      qDelta.x() * (q1.y() - p1.y())) / par;
    const qreal tq = (pDelta.y() * (q1.x() - p1.x()) -
                      pDelta.x() * (q1.y() - p1.y())) / par;

    if (tp<0 || tp>1 || tq<0 || tq>1)
        return intersections;

    const bool p_zero = qFuzzyIsNull(tp);
    const bool p_one = qFuzzyIsNull(tp - 1);

    const bool q_zero = qFuzzyIsNull(tq);
    const bool q_one = qFuzzyIsNull(tq - 1);

    if ((q_zero || q_one) && (p_zero || p_one))
        return intersections;

    QPointF pt;
    if (p_zero) {
        pt = p1;
    } else if (p_one) {
        pt = p2;
    } else if (q_zero) {
        pt = q1;
    } else if (q_one) {
        pt = q2;
    } else {
        pt = q1 + (q2 - q1) * tq;  // intersection = p1 + (p2 - p1) * t
    }
    intersections.push_back(pt);

    return intersections;
}

QVector<QPointF> KisIntersectionFinder::intersectionPoint(cubicBezier c1, Line l2) {

    return intersectionPoint(l2, c1);
}

// function to find intersection points for two buildingBlocks
QVector<QPointF> KisIntersectionFinder::findIntersectionPoints(buildingBlock e1, buildingBlock e2) {
    QVector<QPointF> intersectionPoints;

    if (e1.isCurveTo()) {

        if (e2.isCurveTo()) {
            intersectionPoints = intersectionPoint(e1.getCurveTo(), e2.getCurveTo());
        }
        else {
            intersectionPoints = intersectionPoint(e2.getLineTo(), e1.getCurveTo());
        }
    }

    else {
        if (e2.isCurveTo()) {
            intersectionPoints = intersectionPoint(e1.getLineTo(), e2.getCurveTo());
        }
        else {
            intersectionPoints = intersectionPoint(e1.getLineTo(), e2.getLineTo());
        }
    }

    return intersectionPoints;
}

// finds all the intersection points between two PainterPaths
QVector<QPointF> KisIntersectionFinder::findAllIntersections() {

    QVector<QPointF> allIntersectionPoints;

    // for self intersections of first shape
    for (int i = 0; i < subjectShape.size(); i++) {

        buildingBlock b1 = subjectShape.at(i);
        for (int j = i + 1; j < subjectShape.size(); j++) {

            buildingBlock b2 = subjectShape.at(j);
            QVector<QPointF> rnd = findIntersectionPoints(b1, b2);
            allIntersectionPoints.append(rnd);
        }

        // for intersections with second shape
        for (int k = 0; k < clipShape.size(); k++) {

            buildingBlock b2 = clipShape.at(k);
            QVector<QPointF> rnd = findIntersectionPoints(b1, b2);
            allIntersectionPoints.append(rnd);

        }
    }

    // for self intersections of second shape
    for (int i = 0; i < clipShape.size(); i++) {

        buildingBlock b1 = clipShape.at(i);
        for (int j = i + 1; j < clipShape.size(); j++) {

            buildingBlock b2 = clipShape.at(j);
            QVector<QPointF> rnd = findIntersectionPoints(b1, b2);
            allIntersectionPoints.append(rnd);
        }
    }

//    Q_FOREACH(QPointF pt, allIntersectionPoints) {
//        std::cout << pt.x() << " " << pt.y() << std::endl;
//    }

    return allIntersectionPoints;
}

