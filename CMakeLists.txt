cmake_minimum_required(VERSION 3.5)

project(BezierIntersections LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# --------------------------------------------
#project(mytest LANGUAGES CXX)

find_package(Qt5Test REQUIRED)

#set(CMAKE_INCLUDE_CURRENT_DIR ON)

#set(CMAKE_AUTOMOC ON)

enable_testing(true)

#add_executable(BezierIntersections NumericalEngineTest.cpp)
add_test(NAME BezierIntersections COMMAND NumericalEngineTest)



# -------------------------------------------
# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check https://doc.qt.io/qt/deployment-android.html for more information.
# They need to be set before the find_package( ...) calls below.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

include_directories(/home/tanmay/gsl-install/lib)

find_library(gsl libgsl.a)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets REQUIRED)

set(PROJECT_SOURCES
#        main.cpp

        mainwindow.cpp
        mainwindow.h
        mainwindow.ui
        pathclipper.cpp
        databuffer.h
        bezier.h
        painterpath.h
        painterpath.cpp
        painterpath_p.h
        vectorpath.h
        numericalengine.cpp
        numericalengine.h
        kisintersectionfinder.cpp
        kisintersectionfinder.h

        numericalenginetest.cpp
        numericalenginetest.h
        kisintersectionfindertest.cpp
        kisintersectionfindertest.h

)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(BezierIntersections
        ${PROJECT_SOURCES}
    )
else()
    if(ANDROID)
        add_library(BezierIntersections SHARED
            ${PROJECT_SOURCES}
        )
    else()
        add_executable(BezierIntersections
            ${PROJECT_SOURCES}
        )
    endif()
endif()

target_link_libraries(BezierIntersections PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)
target_link_libraries(BezierIntersections PRIVATE gsl)
target_link_libraries(BezierIntersections PRIVATE Qt${QT_VERSION_MAJOR}::Test)
