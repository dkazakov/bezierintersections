#ifndef PAINTERPATH_P_H
#define PAINTERPATH_P_H

#ifndef MyPainterPath_P_H
#define MyPainterPath_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

//#include <QtGui/private/qtguiglobal_p.h>
//#include "QtGui/MyPainterPath.h"
#include "QtGui/qregion.h"
#include "QtCore/qlist.h"
#include "QtCore/qvarlengtharray.h"

#include <qdebug.h>

//#include <private/qvectorpath_p.h>
//#include <private/qstroker_p.h>

#include <memory>

#include <painterpath.h>
#include <bezier.h>
#include <databuffer.h>
#include <pathclipper.h>
#include <vectorpath.h>
QT_BEGIN_NAMESPACE

// ### Qt 6: merge with MyPainterPathData
class MyPainterPathPrivate
{
public:
    friend class MyPainterPath;
    friend class MyPainterPathData;
    friend class MyPainterPathStroker;
    friend class MyPainterPathStrokerPrivate;
    friend class QMatrix;
    friend class QTransform;
    friend class QVectorPath;
    friend struct MyPainterPathPrivateDeleter;
#ifndef QT_NO_DATASTREAM
    friend Q_GUI_EXPORT QDataStream &operator<<(QDataStream &, const MyPainterPath &);
    friend Q_GUI_EXPORT QDataStream &operator>>(QDataStream &, MyPainterPath &);
#endif

    MyPainterPathPrivate() noexcept
        : ref(1)
    {
    }

    MyPainterPathPrivate(const MyPainterPathPrivate &other) noexcept
        : ref(1),
          elements(other.elements)
    {
    }

    MyPainterPathPrivate &operator=(const MyPainterPathPrivate &) = delete;
    ~MyPainterPathPrivate() = default;

private:
    QAtomicInt ref;
    QVector<MyPainterPath::Element> elements;
};

//class MyPainterPathStrokerPrivate
//{
//public:
//    MyPainterPathStrokerPrivate();

//    QStroker stroker;
//    QVector<qfixed> dashPattern;
//    qreal dashOffset;
//};

class QPolygonF;
class QVectorPathConverter;

class QVectorPathConverter
{
public:
    QVectorPathConverter(const QVector<MyPainterPath::Element> &path, uint fillRule, bool convex)
        : pathData(path, fillRule, convex),
          path(pathData.points.data(), path.size(),
               pathData.elements.data(), pathData.flags) {}

    const QVectorPath &vectorPath() {
        return path;
    }

    struct QVectorPathData {
        QVectorPathData(const QVector<MyPainterPath::Element> &path, uint fillRule, bool convex)
            : elements(path.size()),
              points(path.size() * 2),
              flags(0)
        {
            int ptsPos = 0;
            bool isLines = true;
            for (int i=0; i<path.size(); ++i) {
                const MyPainterPath::Element &e = path.at(i);
                elements[i] = e.type;
                points[ptsPos++] = e.x;
                points[ptsPos++] = e.y;
                if (e.type == MyPainterPath::CurveToElement)
                    flags |= QVectorPath::CurvedShapeMask;

                // This is to check if the path contains only alternating lineTo/moveTo,
                // in which case we can set the LinesHint in the path. MoveTo is 0 and
                // LineTo is 1 so the i%2 gets us what we want cheaply.
                isLines = isLines && e.type == (MyPainterPath::ElementType) (i%2);
            }

            if (fillRule == Qt::WindingFill)
                flags |= QVectorPath::WindingFill;
            else
                flags |= QVectorPath::OddEvenFill;

            if (isLines)
                flags |= QVectorPath::LinesShapeMask;
            else {
                flags |= QVectorPath::AreaShapeMask;
                if (!convex)
                    flags |= QVectorPath::NonConvexShapeMask;
            }

        }
        QVarLengthArray<MyPainterPath::ElementType> elements;
        QVarLengthArray<qreal> points;
        uint flags;
    };

    QVectorPathData pathData;
    QVectorPath path;

private:
    Q_DISABLE_COPY_MOVE(QVectorPathConverter)
};

class MyPainterPathData : public MyPainterPathPrivate
{
public:
    MyPainterPathData() :
        cStart(0),
        fillRule(Qt::OddEvenFill),
        require_moveTo(false),
        dirtyBounds(false),
        dirtyControlBounds(false),
        convex(false),
        pathConverter(nullptr)
    {
    }

    MyPainterPathData(const MyPainterPathData &other) :
        MyPainterPathPrivate(other),
        cStart(other.cStart),
        fillRule(other.fillRule),
        bounds(other.bounds),
        controlBounds(other.controlBounds),
        require_moveTo(false),
        dirtyBounds(other.dirtyBounds),
        dirtyControlBounds(other.dirtyControlBounds),
        convex(other.convex),
        pathConverter(nullptr)
    {
    }

    MyPainterPathData &operator=(const MyPainterPathData &) = delete;
    ~MyPainterPathData() = default;

    inline bool isClosed() const;
    inline void close();
    inline void maybeMoveTo();
    inline void clear();

    const QVectorPath &vectorPath() {
        if (!pathConverter)
            pathConverter.reset(new QVectorPathConverter(elements, fillRule, convex));
        return pathConverter->path;
    }

    int cStart;
    Qt::FillRule fillRule;

    QRectF bounds;
    QRectF controlBounds;

    uint require_moveTo : 1;
    uint dirtyBounds : 1;
    uint dirtyControlBounds : 1;
    uint convex : 1;

    std::unique_ptr<QVectorPathConverter> pathConverter;
};


inline const MyPainterPath QVectorPath::convertToPainterPath() const
{
        MyPainterPath path;
        path.ensureData();
        MyPainterPathData *data = path.d_func();
        data->elements.reserve(m_count);
        int index = 0;
        data->elements[0].x = m_points[index++];
        data->elements[0].y = m_points[index++];

        if (m_elements) {
            data->elements[0].type = m_elements[0];
            for (int i=1; i<m_count; ++i) {
                MyPainterPath::Element element;
                element.x = m_points[index++];
                element.y = m_points[index++];
                element.type = m_elements[i];
                data->elements << element;
            }
        } else {
            data->elements[0].type = MyPainterPath::MoveToElement;
            for (int i=1; i<m_count; ++i) {
                MyPainterPath::Element element;
                element.x = m_points[index++];
                element.y = m_points[index++];
                element.type = MyPainterPath::LineToElement;
                data->elements << element;
            }
        }

        if (m_hints & OddEvenFill)
            data->fillRule = Qt::OddEvenFill;
        else
            data->fillRule = Qt::WindingFill;
        return path;
}

void Q_GUI_EXPORT qt_find_ellipse_coords(const QRectF &r, qreal angle, qreal length,
                                         QPointF* startPoint, QPointF *endPoint);

inline bool MyPainterPathData::isClosed() const
{
    const MyPainterPath::Element &first = elements.at(cStart);
    const MyPainterPath::Element &last = elements.last();
    return first.x == last.x && first.y == last.y;
}

inline void MyPainterPathData::close()
{
    Q_ASSERT(ref.loadRelaxed() == 1);
    require_moveTo = true;
    const MyPainterPath::Element &first = elements.at(cStart);
    MyPainterPath::Element &last = elements.last();

    if (first.x != last.x || first.y != last.y) {
        // qFuzzyCompare checks the difference in values and if below a certain threshold treats them as same by returning true
        if (qFuzzyCompare(first.x, last.x) && qFuzzyCompare(first.y, last.y)) {
            last.x = first.x;
            last.y = first.y;
        }
        else {
            MyPainterPath::Element e = { first.x, first.y, MyPainterPath::LineToElement };
            elements << e;
        }
    }
}

inline void MyPainterPathData::maybeMoveTo()
{
    if (require_moveTo) {
        MyPainterPath::Element e = elements.last();
        e.type = MyPainterPath::MoveToElement;
        elements.append(e);
        require_moveTo = false;
    }
}

inline void MyPainterPathData::clear()
{
    Q_ASSERT(ref.loadRelaxed() == 1);

    elements.clear();

    cStart = 0;
    fillRule = Qt::OddEvenFill;
    bounds = {};
    controlBounds = {};

    require_moveTo = false;
    dirtyBounds = false;
    dirtyControlBounds = false;
    convex = false;

    pathConverter.reset();
}
#define KAPPA qreal(0.5522847498)


QT_END_NAMESPACE

#endif // MyPainterPath_P_H


#endif // PAINTERPATH_P_H
